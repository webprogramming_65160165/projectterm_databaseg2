import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";


@Entity()
export class Material {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  min: string;

  @Column()
  max: string;

  @Column()
  until: string;

  @Column()
  quantity: string;

  @Column({ default: "noimage.jpg" })
  image: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  update: Date;
}
