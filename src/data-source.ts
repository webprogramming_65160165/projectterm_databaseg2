import "reflect-metadata"
import { DataSource } from "typeorm"
import { User } from "./entity/User"
import { Role } from "./entity/Role"
import { Order } from "./entity/Order"
import { OrderItem } from "./entity/OrderItem"
import { Product } from "./entity/Product"
import { Type } from "./entity/Type"

export const AppDataSource = new DataSource({
    type: "sqlite",
    database: "database.sqlite",
    synchronize: true,
    logging: false,
    entities: [User, Role, Order, OrderItem, Product, Type],
})
